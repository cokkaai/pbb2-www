// vim: set ts=4 sw=4 expandtab:

const { src, dest, parallel, watch } = require('gulp');
const minifyCSS = require('gulp-csso');
const concat = require('gulp-concat');
// const uglify = require('gulp-uglify');
const del = require('del');


// Copy contents to dist directory
function distContents(contents) {
    function copy(content) {
        if (!content.src) {
            console.warning("Content src is not defined.");
        }

        if (!content.dest) {
            console.warning("Content dest is not defined.");
        }

        src(content.src)
            .pipe(dest(content.dest));
    }

    if (contents) {
        if (Array.isArray(contents)) {
            contents.forEach(content => copy(content));
        } else {
            copy(contents);
        }
    }
}

// Assemble css in the dist directory
function distCss(css) {
    if (css) {
        if (!css.src) {
            console.warning("CSS src is not defined.");
            return;
        }

        if (!css.dest) {
            console.warning("CSS dest is not defined.");
            return;
        }

        src(css.src)
            .pipe(minifyCSS())
            .pipe(dest(css.dest));
    }
}

// Assemble scripts and copy to dist directory
function distScripts(scripts) {
    if (scripts) {
        if (!scripts.src) {
            console.warning("Scripts src is not defined");
            return;
        }

        if (!scripts.dest) {
            console.warning("Scripts dest is not defined");
            return;
        }

        if (!scripts.name) {
            console.warning("Scripts name is not defined");
            return;
        }

        src(scripts.src, { sourcemaps: true })
            //.pipe(uglify())
            .pipe(concat(scripts.name))
            .pipe(dest(scripts.dest, { sourcemaps: "." }));
    }
}

// Assemble the full module in the dist directory
function distModule(module) {
    if (module) {
        distScripts(module.scripts);
        distCss(module.css);
        distContents(module.contents)
            /*;
                    parallel(
                        () => { if (module.scripts) distScripts(module.scripts); },
                        () => { if (module.css) distCss(module.css); },
                        () => { if (module.contents) distContents(module.contents); }
                    );
                    */
    }
}

function clearModule(module) {
    var products = [];

    if (!module) {
        return;
    }

    if (module.scripts && module.scripts.dest) {
        products.push(module.scripts.dest);
    }

    if (module.css && module.css.dest) {
        products.push(module.scripts.dest);
    }

    if (module.contents && module.contents.dest) {
        products.push(module.scripts.dest);
    }

    if (products.lenght > 0) {
        del(products);
    }
}

function moduleSources(module) {
    var sources = [];

    function append(tail) {
        if (Array.isArray(tail)) {
            sources = sources.concat(tail);
        } else {
            sources.push(tail);
        }
    }

    if (module) {
        if (module.scripts && module.scripts.src) {
            append(module.scripts.src);
        }

        if (module.css && module.css.src) {
            append(module.css.src);
        }

        if (module.contents && module.contents.src) {
            append(module.contents.src);
        }
    }

    return sources;
}

// === Define my app ===

const modules = {
    app: {
        scripts: {
            src: ["src/app/js/**/*.js", "src/js/services.js"],
            dest: "dist/app/js",
            name: "app.min.js"
        },
        css: {
            src: "src/css/*.css",
            dest: "dist/css"
        },
        contents: {
            src: ["src/app/**/*.html"],
            dest: "dist/app"
        }
    },
    preview: {
        scripts: {
            src: ["src/js/services.js", "src/preview/js/preview.js"],
            dest: "dist/preview/js",
            name: "preview.min.js"
        },
        contents: {
            src: ["src/preview/**/*.html"],
            dest: "dist/preview"
        }
    },
    show: {
        scripts: {
            src: ["src/js/services.js", "src/show/js/show.js"],
            dest: "dist/show/js",
            name: "show.min.js"
        },
        contents: {
            src: ["src/show/**/*.html"],
            dest: "dist/show"
        }
    },
    bundle: {
        scripts: {
            src: [
                'node_modules/angular/angular.min.js',
                'node_modules/angular-animate/angular-animate.min.js',
                'node_modules/angular-aria/angular-aria.min.js',
                'node_modules/angular-messages/angular-messages.min.js',
                'node_modules/angular-material/angular-material.min.js',
                'node_modules/angular-route/angular-route.min.js'
            ],
            dest: "dist/js",
            name: "angular-bundle.min.js"
        },
        contents: [{
            src: ['node_modules/angular-material/angular-material.min.css',
                'node_modules/angular-material/modules/js/fabSpeedDial/fabSpeedDial.min.css'
            ],
            dest: "dist/css"
        }, {
            src: "src/img/**",
            dest: "dist/img"
        }, {
            src: "src/templates/**",
            dest: "dist/templates"
        }, {
            src: "src/favicon.ico",
            dest: "dist"
        }]
    }
};

// === Watch for modules changes ===

watch(moduleSources(modules.app), function(cb) {
    distModule(modules.app);
    cb();
});

watch(moduleSources(modules.preview), function(cb) {
    distModule(modules.preview);
    cb();
});

watch(moduleSources(modules.show), function(cb) {
    distModule(modules.show);
    cb();
});

// TODO: Watch for bundle changes

// === Clear the dist directory ===

function clear() {
    clearModule(modules.app);
    clearModule(modules.show);
    clearModule(modules.preview);
    clearModule(modules.bundle);
}

exports.default = parallel(
    function distBundle() {
        distModule(modules.bundle);
    },
    function distPreviewModule() {
        distModule(modules.preview);
    },
    function distShowModule() {
        distModule(modules.show);
    },
    function distAppModule() {
        distModule(modules.app);
    }
);