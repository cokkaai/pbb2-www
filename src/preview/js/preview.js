// vim: set ts=4 sw=4 expandtab:

(function() {
    "use strict";

    var app = angular.module("PreviewApp", [
        "ngMaterial",
        "services"
    ]);

    app.controller('PreviewCtrl', ["$scope", "$timeout", "$log", "$window", "store",
        function($scope, $timeout, $log, $window, store) {
            $scope.data = {
                pharmacy: {
                    name: null,
                    address: null,
                    phone: null
                },
                message: null,
                documents: [],
                clock: {
                    display: null,
                    time: new Date()
                },
                color_theme: null,
                version: null
            };

            $scope.displayedDocument = null;

            $scope.displayTime = 15000;

            $scope.toolbar = {
                isOpen: false,
                direction: "right"
            };

            $scope.close = function() {
                $scope.toolbar.isOpen = false;
            };

            $scope.back = function() {
                $window.location = "/app/index.html";
            };

            $scope.publish = function() {
                store.updateRelease(function(response) {
                    $window.alert("Presentazione pubblicata sulla bacheca");
                    $log.info("Presentazione pubblicata sulla bacheca");
                });
            };

            $scope.nextRun = null;

            function isUpdatedConfiguration(response) {
                // Received a configuration
                if (response && response.data) {
                    // No previous configuration
                    if (!$scope.data.version) {
                        return true;
                    }

                    // Received a new configuration
                    if ($scope.data.version && response.data.version != $scope.data.version) {
                        return true;
                    }
                }

                return false;
            }

            function scheduleNextRun() {
                if ($scope.nextRun) {
                    $timeout.cancel($scope.nextRun);
                }

                $scope.nextRun = $timeout(refresh, $scope.displayTime);
            }

            function refresh() {
                $log.debug("Updating page");

                store.preview(
                    // Update page data frmo API and schedule next refresh
                    function(response) {
                        if (isUpdatedConfiguration(response)) {
                            updatePharmacy(response.data);
                            updateMessage(response.data);
                            updateShowTime(response.data);
                            updateClockPreference(response.data);
                            updateColorTheme(response.data);
                            updateDocuments(response.data);

                            $scope.data.version = response.data.version;
                        }

                        updateClockTime();
                        setDocumentToDisplay();
                        scheduleNextRun();
                    },
                    // On API error log on console and schedule next refresh
                    function(error) {
                        if (error.status === -1 || error.xhrStatus === "error") {
                            $log.error("L'API non ha risposto");
                        } else {
                            $log.error(error);
                        }

                        updateClockTime();
                        setDocumentToDisplay();
                        scheduleNextRun();
                    });
            }

            function init() {
                $log.log("PreviewCtrl starting");

                refresh();
            }

            function updatePharmacy(config) {
                var dirty = false;

                if (config && config.my_pharmacy) {
                    if ($scope.data.pharmacy.name !== config.my_pharmacy.name) {
                        $scope.data.pharmacy.name = config.my_pharmacy.name;
                        dirty = 1;
                    }

                    if ($scope.data.pharmacy.address !== config.my_pharmacy.address) {
                        $scope.data.pharmacy.address = config.my_pharmacy.address;
                        dirty = 1;
                    }

                    if ($scope.data.pharmacy.phone !== config.my_pharmacy.phone) {
                        $scope.data.pharmacy.phone = config.my_pharmacy.phone;
                        dirty = 1;
                    }

                    $log.log("Updated data: pharmacy");
                }
            }

            function updateMessage(config) {
                if (config && config.message) {
                    if ($scope.data.message !== config.message) {
                        $scope.data.message = config.message;
                        $log.log("Updated data: message");
                    }
                }
            }

            function updateShowTime(config) {
                if (config && config.show_time) {
                    if ($scope.displayTime !== 1000 * config.show_time) {
                        $scope.displayTime = 1000 * config.show_time;
                        $log.log("Updated data: show time");
                    }
                }
            }

            function updateClockPreference(config) {
                if (config && config.clock) {
                    $scope.data.clock.display = config.clock;

                    $log.log("Updated data: clock preference");
                }
            }

            function updateColorTheme(config) {
                if (config && config.color_theme) {
                    if ($scope.data.color_theme !== config.color_theme) {
                        $scope.data.color_theme = config.color_theme;
                        $log.log("Updated data: color theme");
                    }
                }
            }

            function updateDocuments(config) {
                if (config && config.documents) {
                    $scope.data.documents = [];

                    for (var i = 0; i < config.documents.length; i++) {
                        var desc = null;
                        var url = null;

                        if (config.documents[i].file_name) {
                            url = config.documents[i].file_name.trim();
                        }

                        if (config.documents[i].description) {
                            desc = config.documents[i].description.trim();
                        }

                        $scope.data.documents.push({
                            backgroundImage: "url(/images/" + url + ")",
                            description: desc
                        });
                    }

                    $scope.displayedDocument = null;
                    $log.log("Updated data: documents");
                }
            }

            function setDocumentToDisplay() {
                var previous = $scope.displayedDocument;

                if ($scope.data.documents && $scope.data.documents.length > 0) {
                    if ($scope.displayedDocument === null) {
                        $scope.displayedDocument = 0;
                    } else if ($scope.displayedDocument === $scope.data.documents.length - 1) {
                        $scope.displayedDocument = 0;
                    } else {
                        $scope.displayedDocument++;
                    }
                }

                $log.debug("Document to display (index): " + previous + " -> " + $scope.displayedDocument);
            }

            function updateClockTime() {
                if ($scope.data.clock.display) {
                    $scope.data.clock.time = new Date();
                }
            }

            init();
        }
    ]);

})();