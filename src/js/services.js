// vim: set ts=4 sw=4 expandtab:
(function() {
    'use strict';

    var services = angular.module("services", []);

    /**
     * Generates a full URL for the REST endpoint
     * @param {String} url    The REST endpoint URL relative to site root
     * @returns {String}      The REST endpoint full URL
     */
    services.factory("host", [
        function() {
            return {
                url: function(url) {
                    // var host = "http://localhost:8000/api/";
                    var host = "/api/";

                    //if (url.substring(url.length - 1) !== "/")
                    //    url += "/";

                    return host + url;
                }
            };
        }
    ]);

    services.factory("store", ["$log", "$http", "host",
        function($log, $http, host) {
            var _api = {
                pharmacy: host.url("draft/pharmacy"),
                shift: host.url("draft/shift"),
                calendar: host.url("draft/calendar"),
                presentation: host.url("draft/presentation"),
                image: host.url("draft/image"),
                document: host.url("draft/document"),
                release: host.url("release"),
                preview: host.url("preview"),
                show: host.url("show"),
                log: {
                    info: host.url("log/info"),
                    warn: host.url("log/warn"),
                    error: host.url("log/error"),
                }
            };

            const timeoutMs = 10000;

            function _config(method, url, data, params) {
                var config = {
                    method: method,
                    url: url,
                    timeout: timeoutMs
                };

                if (data) {
                    config.data = data;
                }

                if (params) {
                    config.params = params;
                }

                return config;
            }

            function _get(url, params) {
                return _config("GET", url, null, params);
            }

            function _post(url, data) {
                return _config("POST", url, data);
            }

            function _put(url, data) {
                return _config("PUT", url, data);
            }

            function _delete(url) {
                return _config("DELETE", url);
            }

            function _readPharmacies(callback, error) {
                $http(_get(_api.pharmacy))
                    .then(callback, error);
            }

            function _readPharmacy(id, callback, error) {
                $http(_get(_api.pharmacy + "/" + id))
                    .then(callback, error);
            }

            function _createPharmacy(pharmacy, callback, error) {
                $http(_post(_api.pharmacy, pharmacy))
                    .then(callback, error);
            }

            function _updatePharmacy(pharmacy, callback, error) {
                $http(_put(_api.pharmacy + "/" + pharmacy.id, pharmacy))
                    .then(callback, error);
            }

            function _deletePharmacy(id, callback, error) {
                $http(_delete(_api.pharmacy + "/" + id))
                    .then(callback, error);
            }

            function _readShift(callback, error) {
                $http(_get(_api.shift))
                    .then(callback, error);
            }

            function _updateShift(shift, callback, error) {
                $http(_put(_api.shift, shift))
                    .then(callback, error);
            }

            function _readCalendar(start, end, callback, error) {
                $http(_get(_api.calendar, {
                        start_date: start,
                        end_date: end
                    }))
                    .then(callback, error);
            }

            function _readDocuments(callback, error) {
                $http(_get(_api.document))
                    .then(callback, error);
            }

            function _readDocument(id, callback, error) {
                $http(_get(_api.document + "/" + id))
                    .then(callback, error);
            }

            function _createDocument(document, callback, error) {
                $http(_post(_api.document, document))
                    .then(callback, error);
            }

            function _updateDocument(document, callback, error) {
                $http(_put(_api.document + "/" + document.id, document))
                    .then(callback, error);
            }

            function _deleteDocument(id, callback, error) {
                $http(_delete(_api.document + "/" + id))
                    .then(callback, error);
            }

            function _createImage(blob, callback, error) {
                $http(_post(_api.image, blob))
                    .then(callback, error);
            }

            function _deleteImage(id, callback, error) {
                $http(_delete(_api.image + "/" + id))
                    .then(callback, error);
            }

            function _readPresentation(callback, error) {
                $http(_get(_api.presentation))
                    .then(callback, error);
            }

            function _updatePresentation(presentation, callback, error) {
                $http(_put(_api.presentation, presentation))
                    .then(callback, error);
            }

            function _preview(callback, error) {
                $http(_get(_api.preview))
                    .then(callback, error);
            }

            function _readRelease(callback, error) {
                $http(_get(_api.release))
                    .then(callback, error);
            }

            function _updateRelease(callback, error) {
                $http(_put(_api.release))
                    .then(callback, error);
            }

            function _show(callback, error) {
                $http(_get(_api.show))
                    .then(callback, error);
            }

            function _logInfo(data) {
                $log.info(data);
                $http(_post(_api.log.info, data));
            }

            function _logWarn(data) {
                $log.warn(data);
                $http(_post(_api.log.warn, data));
            }

            function _logError(data) {
                $log.error(data);
                $http(_post(_api.log.error, data));
            }

            return {
                readPharmacies: _readPharmacies,
                readPharmacy: _readPharmacy,
                createPharmacy: _createPharmacy,
                updatePharmacy: _updatePharmacy,
                deletePharmacy: _deletePharmacy,

                readShift: _readShift,
                updateShift: _updateShift,

                readCalendar: _readCalendar,

                readPresentation: _readPresentation,
                updatePresentation: _updatePresentation,

                readDocuments: _readDocuments,
                readDocument: _readDocument,
                createDocument: _createDocument,
                updateDocument: _updateDocument,
                deleteDocument: _deleteDocument,

                createImage: _createImage,
                deleteImage: _deleteImage,

                readRelease: _readRelease,
                updateRelease: _updateRelease,

                preview: _preview,
                show: _show,

                logInfo: _logInfo,
                logWarn: _logWarn,
                logError: _logError,
            };
        }
    ]);

    services.factory("backendLogger", ["$log", "host", "context",
        function($log, host, context) {
            function _data(message) {
                return {
                    message: message,
                    context: context.data()
                };
            }

            function _logInfo(message) {
                var data = _data(message);
                $log.info(data);
                _post(host.url("log/info"), data);
            }

            function _logWarn(message) {
                var data = _data(message);
                $log.warn(data);
                _post(host.url("log/warn"), data);
            }

            function _logError(message) {
                var data = _data(message);
                $log.error(data);
                _post(host.url("log/error"), data);
            }

            function _post(url, data) {
                try {
                    var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");

                    xhr.open('POST', url);

                    xhr.onreadystatechange = function() {
                        if (xhr.readyState > 3) {
                            if (xhr.status != 200) {
                                $log.error("Response status is " + xhr.status + ". Unable to log on the backend");
                            }
                        }
                    };

                    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                    xhr.setRequestHeader('Content-Type', 'application/json');
                    xhr.send(JSON.stringify(data));

                    return xhr;
                } catch (e) {
                    // TODO: log me somewhere
                }
            }

            return {
                info: _logInfo,
                warn: _logWarn,
                error: _logError,
            };
        }
    ]);

    // The $exceptionHandler cannot use the $rootScope because
    // $rootScope itself depends on $exceptionHandler.
    services.factory('$exceptionHandler', ["backendLogger", "context",
        function(backendLogger, context) {
            return function myExceptionHandler(exception, cause) {
                if (exception) {
                    context.fromException(exception);

                    if (cause) {
                        context.update("cause", cause);
                    }

                    try {
                        backendLogger.error(exception.message);
                    } finally {
                        context.clear();
                    }
                }
            };
        }
    ]);

    services.factory('context', [function() {
        var data = [];

        function _item(key, value) {
            return {
                name: key,
                data: value
            };
        }

        function _update(key, value) {
            var i = data.findIndex(x => x.hasOwnProperty("name") && x.name === key);

            if (i >= 0) {
                data[i].data = value;
            } else {
                data.push(_item(key, value));
            }
        }

        function _clear() {
            data = [];
        }

        function _data() {
            return data;
        }

        function _fromException(exception) {
            if (exception && exception.fileName) {
                _update("fileName", exception.fileName);
            }

            if (exception && exception.lineNumber) {
                _update("lineNumber", exception.lineNumber.toString());
            }

            if (exception && exception.columnNumber) {
                _update("columnNumber", exception.columnNumber.toString());
            }

            if (exception && exception.name) {
                _update("name", exception.name);
            }

            if (exception && exception.stack) {
                _update("stack", exception.stack);
            }
        }

        function _fromRequest(request) {
            _clear();

            if (request) {
                if (request.config && request.config.method) {
                    _update("method", request.config.method);
                }

                if (request.config && request.config.url) {
                    _update("url", request.config.url);
                }

                if (request.status) {
                    _update("status", request.status.toString());
                }

                if (request.statusText) {
                    _update("text", request.statusText);
                }

                if (request.xhrStatus) {
                    _update("xhrStatus", request.xhrStatus.toString());
                }
            }
        }

        return {
            update: _update,
            clear: _clear,
            data: _data,
            fromException: _fromException,
            fromRequest: _fromRequest
        };
    }]);
})();