// vim: set ts=4 sw=4 expandtab:

(function () {
    "use strict";

    var app = angular.module("PharmacyBulletinBoardApp", [
        "ngMaterial",
        "ngRoute",
        "services",
        "controllers",
        "directives"
    ]);

    app.config(["$routeProvider",
        function ($routeProvider) {
            $routeProvider.
                when("/app", {
                    templateUrl: "partials/app.html"
                }).
                when("/pharmacy", {
                    templateUrl: "partials/pharmacy/list.html",
                    controller: "PharmaciesCtrl"
                }).
                when("/pharmacy/:id", {
                    templateUrl: "partials/pharmacy/edit.html",
                    controller: "EditPharmacyCtrl"
                }).
                when("/pharmacy/new", {
                    templateUrl: "partials/pharmacy/edit.html",
                    controller: "EditPharmacyCtrl"
                }).
                when("/shifts", {
                    templateUrl: "partials/shifts/ordering.html",
                    controller: "ShiftCtrl"
                }).
                when("/calendar", {
                    templateUrl: "partials/calendar/calc.html",
                    controller: "CalendarCtrl"
                }).
                when("/presentation", {
                    templateUrl: "partials/presentation/options.html",
                    controller: "PresentationCtrl"
                }).
                when("/document", {
                    templateUrl: "partials/document/list.html",
                    controller: "DocumentsCtrl"
                }).
                when("/document/:id", {
                    templateUrl: "partials/document/edit.html",
                    controller: "EditDocumentCtrl"
                }).
                otherwise({
                    redirectTo: "/app"
                });
        }
    ]);


})();

