(function() {
    'use strict';

    var controllers = angular.module("controllers");

    controllers.controller("DocumentsCtrl", ["$scope", "$log", "$location", "$rootScope", "context", "store",
        function($scope, $log, $location, $rootScope, context, store) {
            $scope.documents = null;
            $scope.status = new FormStatus("Caricamento dati in corso...");

            $scope.edit = function(document) {
                $location.url("/document/" + document.id);
            };

            $scope.add = function() {
                $location.url("/document/_new");
            };

            $scope.makeUrl = function(image) {
                return "/thumbs/" + image;
            };

            function init() {
                store.readDocuments(function(response) {
                    if (response.status === 200 && response.data && response.data.length > 0) {
                        $scope.status.readyWithData();
                        $scope.documents = response.data;
                    } else {
                        $scope.status.ready();
                    }
                }, function(request) {
                    _manageError("Impossibile leggere i dati", request);
                });

                $log.log("Init DocumentsCtrl");
            }

            function _manageError(error, request) {
                if (request) {
                    if (request.xhrStatus === "timeout") {
                        error += ": il server non ha risposto";
                    } else if (request.xhrStatus !== "complete") {
                        error += ": il server ha generato un errore";
                    }

                    context.fromRequest(request);
                }

                $scope.status.error(error);
                $rootScope.$emit("error_occurred", new Error(error));
            }

            init();
        }
    ]);
}());