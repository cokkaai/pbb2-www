(function() {
    'use strict';

    var controllers = angular.module("controllers");

    controllers.controller("SidenavCtrl", ["$scope", "$location", "$rootScope", "context",
        function($scope, $location, $rootScope, context) {
            $scope.app = function() {
                $location.url("/app");
                $scope.toggleLeft();
            };
            $scope.pharmacies = function() {
                $location.url("/pharmacy");
                $scope.toggleLeft();
            };
            $scope.shifts = function() {
                $location.url("/shifts");
                $scope.toggleLeft();
            };
            $scope.calendar = function() {
                $location.url("/calendar");
                $scope.toggleLeft();
            };
            $scope.presentation = function() {
                $location.url("/presentation");
                $scope.toggleLeft();
            };
            $scope.documents = function() {
                $location.url("/document");
                $scope.toggleLeft();
            };
            $scope.preview = function() {
                var windowFeatures = "menubar=no,location=yes,resizable=yes,scrollbars=no,status=no";
                var target = "_self";
                window.open("/preview/index.html", target, windowFeatures);
            };
            $scope.bomb = function() {
                var e = new Error("Sbombazza a bestia");
                context.clear();
                context.update("property", "value");
                $rootScope.$emit("error_occurred", e);
            };
        }
    ]);
}());