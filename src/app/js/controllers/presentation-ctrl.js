(function() {
    'use strict';

    var controllers = angular.module("controllers");

    controllers.controller("PresentationCtrl", ["$scope", "$log", "$mdColors", "$mdColorPalette", "$rootScope", "$timeout", "store", "context",
        function($scope, $log, $mdColors, $mdColorPalette, $rootScope, $timeout, store, context) {
            $scope.colors = Object.keys($mdColorPalette);

            $scope.status = new FormStatus("Caricamento dati in corso...");

            $scope.presentation = {
                data: {
                    daily_opening_text: "",
                    on_duty_text: "",
                    show_time: 15,
                    fade_time: 3,
                    color_theme: defaultColor(),
                    clock: true
                },
                delayedSave: function() {
                    if ($scope.status.saving !== null) {
                        $timeout.cancel($scope.presentation.saving);
                    }
                    $scope.presentation.saving = $timeout($scope.presentation.save, 500);
                },
                save: function() {
                    if ($scope.presentation.data) {
                        store.updatePresentation($scope.presentation.data,
                            function() {} /*callback*/ ,
                            function(response) /*error*/ {
                                var error = "Impossibile salvare la configurazione della presentazione." +
                                    " Si è verificato un erroer sul server o la richiesta non è arrivata in tempo." +
                                    " Riprova a salvare o ricarica la pagina per annullare (perderai le modifiche).";

                                _manageError(error, request);
                            });
                    }
                }
            };

            function readPresentation() {
                store.readPresentation(function(response) {
                    if (response.data) {
                        $scope.status.readyWithData();
                        $scope.presentation.data = response.data;
                    }
                }, function(request) {
                    var error = "Impossibile leggere la configurazione della presentazione." +
                        " Stai vedendo la configurazione predefinita." +
                        " Ricarica la pagina per riprovare o salva i dati iniziali per riconfigurare";

                    _manageError(error, request);
                });
            }

            function defaultColor() {
                if ($scope.colors) {
                    return $scope.colors[0];
                } else {
                    return "brown";
                }
            }

            function init() {
                readPresentation();
                $log.log("Init PresentationCtrl");
            }

            function _manageError(error, request) {
                if (request) {
                    if (request.xhrStatus === "timeout") {
                        error += ": il server non ha risposto";
                    } else if (request.xhrStatus !== "complete") {
                        error += ": il server ha generato un errore";
                    }

                    context.fromRequest(request);
                }

                $scope.status.error(error);
                $rootScope.$emit("error_occurred", new Error(error));
            }

            init();
        }
    ]);

}());