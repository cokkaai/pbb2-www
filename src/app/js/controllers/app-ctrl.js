(function() {
    'use strict';

    var controllers = angular.module("controllers");

    controllers.controller('AppCtrl', ["$scope", "$timeout", "$mdSidenav", "$log", "$rootScope", "$exceptionHandler", "context",
        function($scope, $timeout, $mdSidenav, $log, $rootScope, $exceptionHandler, context) {
            $scope.toolbarLabel = "Bacheca farmacia";
            $scope.toggleLeft = buildDelayedToggler('left');
            $scope.error = null;

            $scope.reportError = function() {
                window.alert("Davide ne ha fatta un'altra!!!\nCopia il testo sotto e inviaglielo.\n---\n" + $scope.error);
                $scope.error = null;
            };

            /**
             * Supplies a function that will continue to operate until the
             * time is up.
             */
            function debounce(func, wait, context) {
                var timer;

                return function debounced() {
                    var context = $scope,
                        args = Array.prototype.slice.call(arguments);
                    $timeout.cancel(timer);
                    timer = $timeout(function() {
                        timer = undefined;
                        func.apply(context, args);
                    }, wait || 10);
                };
            }

            /**
             * Build handler to open/close a SideNav; when animation finishes
             * report completion in console
             */
            function buildDelayedToggler(navID) {
                return debounce(function() {
                    // Component lookup should always be available since we are not using `ng-if`
                    $mdSidenav(navID)
                        .toggle();
                    // .then(function () {
                    //     $log.debug("toggle " + navID + " is done");
                    // };
                }, 200);
            }

            $rootScope.$on("error_occurred", function(_event, exception) {
                if (exception) {
                    var error = exception.message;

                    context.fromException(exception);

                    if (context.data().length > 0) {
                        error += "\n" + JSON.stringify(context.data());
                    }

                    $scope.error = error;
                    $exceptionHandler(exception);
                }
            });
        }
    ]);
}());