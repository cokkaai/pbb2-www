(function() {
    'use strict';

    var controllers = angular.module("controllers");

    controllers.controller("ShiftCtrl", ["$scope", "$log", "store",
        function($scope, $log, store) {
            function _manageError(error, request) {
                if (request) {
                    if (request.xhrStatus === "timeout") {
                        error += ": il server non ha risposto";
                    } else if (request.xhrStatus !== "complete") {
                        error += ": il server ha generato un errore";
                    }

                    context.fromRequest(request);
                }

                $scope.status.error(error);
                $rootScope.$emit("error_occurred", new Error(error));
            }

            $scope.shift = {
                status: new FormStatus("Caricamento turni in corso"),
                data: null,
                save: function() {
                    if ($scope.shift.data) {
                        store.updateShift($scope.shift.data,
                            function() {},
                            function(request) {
                                _manageError("Impossibile aggiornare i dati dell turno: il server non ha risposto", request);
                            });
                    }
                }
            };

            $scope.pharmacies = {
                status: new FormStatus("Caricamento farmacie in corso"),
                data: null
            };

            $scope.calendar = {
                status: new FormStatus("Caricamento calendario in corso"),
                data: null,
                select: null,
                swap: function(indexOld, idNew) {
                    var to = $scope.calendar.data[indexOld].pharmacy;
                    var indexNew = null;
                    for (var i = 0; i < $scope.calendar.data.length; i++) {
                        if (idNew === $scope.calendar.data[i].pharmacy.id) {
                            indexNew = i;
                        }
                    }

                    $scope.calendar.data[indexOld].pharmacy = $scope.calendar.data[indexNew].pharmacy;
                    $scope.calendar.data[indexOld].pharmacy.sequence = indexOld;
                    $scope.calendar.select[indexOld] = $scope.calendar.data[indexOld].pharmacy.id;

                    $scope.calendar.data[indexNew].pharmacy = to;
                    $scope.calendar.data[indexNew].pharmacy.sequence = indexNew;
                    $scope.calendar.select[indexNew] = $scope.calendar.data[indexNew].pharmacy.id;

                    store.updatePharmacy($scope.calendar.data[indexOld].pharmacy);
                    store.updatePharmacy($scope.calendar.data[indexNew].pharmacy);
                },
                load: function() {
                    if (!$scope.pharmacies.data) {
                        $scope.pharmacies.data = null;
                        return;
                    }

                    var length = $scope.pharmacies.data.length * $scope.shift.data.days - 1;
                    var start = new Date();
                    var end = new Date(new Date().setDate(start.getDate() + length));

                    $scope.calendar.status.loading = true;

                    store.readCalendar(start, end, function(response) {
                        $scope.calendar.data = response.data.shifts.slice(0, $scope.pharmacies.data.length);
                        $scope.calendar.select = $scope.calendar.data.map(x => x.pharmacy.id);
                        $scope.calendar.status.readyWithData();
                    }, function() {
                        $scope.calendar.status.ready("Nessun dato caricato per il calendario");
                    });
                }
            };

            function readPharmacies() {
                store.readPharmacies(function(response) {
                    if (response.data.length > 0) {
                        $scope.pharmacies.data = response.data.sort(function(a, b) {
                            if (a.name < b.name) {
                                return -1;
                            } else {
                                return 1;
                            }
                        });
                        $scope.pharmacies.status.readyWithData();
                        $scope.calendar.load();
                    } else {
                        $scope.pharmacies.status.ready();
                    }
                    $scope.pharmacies.loading = false;
                });
            }

            function readShift() {
                store.readShift(function(response) {
                    $scope.shift.data = {
                        start: new Date(response.data.start),
                        days: response.data.days
                    };
                    $scope.shift.status.readyWithData();
                }, function() {
                    $scope.shift.data = {
                        start: new Date(),
                        days: 7
                    };
                    $scope.shift.save();
                    $scope.shift.status.readyWithData();
                });
            }

            function init() {
                readShift();
                readPharmacies();
                $log.log("Init ShiftCtrl");
            }

            init();
        }
    ]);

}());