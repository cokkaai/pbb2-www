(function() {
    'use strict';

    var controllers = angular.module("controllers");

    controllers.controller("EditPharmacyCtrl", ["$scope", "$log", "$location", "$routeParams", "store", "context",
        function($scope, $log, $location, $routeParams, store, context) {
            $scope.pharmacy = null;
            $scope.status = new FormStatus("Caricamento dati in corso...");

            function _manageError(error, request) {
                if (request) {
                    if (request.xhrStatus === "timeout") {
                        error += ": il server non ha risposto";
                    } else if (request.xhrStatus !== "complete") {
                        error += ": il server ha generato un errore";
                    }

                    context.fromRequest(request);
                }

                $scope.status.error(error);
                $rootScope.$emit("error_occurred", new Error(error));
            }

            $scope.exit = function() {
                var ok = true;

                if ($scope.status.isDirty) {
                    ok = confirm("Vuoi annullare le modifiche?");
                }

                if (ok) {
                    $location.url("/pharmacy");
                }
            };

            $scope.flip = function() {
                $scope.pharmacy.mine = !$scope.pharmacy.mine;
                $scope.status.dirty();
            };

            $scope.save = function() {
                $log.log("Saving");
                $scope.status.saving();

                if ($scope.pharmacy.id === null) {
                    store.createPharmacy($scope.pharmacy, function(response) {
                        if (response.status === 200) {
                            $log.log("Created pharmacy " + response.headers().location);
                            $location.url("/pharmacy");
                        } else {}
                    }, function() {
                        var error = "Impossibile leggere i dati della farmacia: il server non ha risposto";
                        _manageError(error);
                    });
                } else {
                    store.updatePharmacy($scope.pharmacy, function(response) {
                        if (response.status === 200) {
                            $log.log("Updated pharmacy " + $scope.pharmacy.id);
                            $location.url("/pharmacy");
                        }
                    }, function(request) {
                        var error = "Impossibile aggiornare i dati della farmacia: il server non ha risposto";
                        _manageError(error, request);
                    });
                }

                $scope.status.ready();
            };

            $scope.delete = function() {
                if ($routeParams.id === "_new") {
                    init();
                } else if (confirm("Vuoi cancellare la farmacia?")) {
                    store.deletePharmacy($scope.pharmacy.id, function(response) {
                        if (response.status === 200) {
                            $log.log("Deleted pharmacy " + $scope.pharmacy.id);
                            $location.url("/pharmacy");
                        }
                    }, function(request) {
                        if (request && request.status === 404) {
                            $log.log("Pharmacy " + $scope.pharmacy.id + " already deleted");
                            $location.url("/pharmacy");
                        } else {
                            var error = "Impossibile cancellare la farmacia: il server non ha risposto";
                            _manageError(error, request);
                        }
                    });
                }
            };

            function init() {
                if ($routeParams.id === "_new") {
                    $scope.pharmacy = {
                        id: null,
                        name: null,
                        address: null,
                        sequence: 0,
                        phone: null,
                        mine: false
                    };
                    $scope.status.ready();
                } else {
                    store.readPharmacy($routeParams.id, function(response) {
                        if (response.status === 200) {
                            $scope.pharmacy = response.data;
                            $scope.status.readyWithData();
                        }
                    });
                }

                $log.log("Init EditPharmacyCtrl");
            }

            init();
        }
    ]);

}());