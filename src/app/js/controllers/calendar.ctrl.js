(function() {
    'use strict';

    var controllers = angular.module("controllers");

    controllers.controller("CalendarCtrl", ["$scope", "$log", "$rootScope", "store", "context",
        function($scope, $log, $rootScope, store, context) {
            $scope.start = null;
            $scope.end = null;
            $scope.calendar = null;
            $scope.status = null;

            function query(start, end) {
                store.readCalendar(start, end, function(response) {
                    if (response.status === 200) {
                        if (response.data && response.data.shifts && response.data.shifts.length > 0) {
                            $scope.calendar = response.data;
                            $scope.status.readyWithData();
                        } else {
                            $scope.status.ready("Nessun turno da visualizzare. Hai censito le farmacie?");
                        }
                    }
                }, function(request) {
                    var error = "Impossibile calcolare il calendario: il server non ha risposto";

                    _manageError(error, request);
                });
            }

            $scope.query = function() {
                if ($scope.start && $scope.end) {
                    $scope.status = new FormStatus("Caricamento dati in corso...");
                    $scope.calendar = null;
                    query($scope.start, $scope.end);
                }
            };

            function _manageError(error, request) {
                if (request) {
                    if (request.xhrStatus === "timeout") {
                        error += ": il server non ha risposto";
                    } else if (request.xhrStatus !== "complete") {
                        error += ": il server ha generato un errore";
                    }

                    context.fromRequest(request);
                }

                $scope.status.error(error);
                $rootScope.$emit("error_occurred", new Error(error));
            }

            $log.log("Init CalendarCtrl");
        }
    ]);

}());