(function() {
    'use strict';

    var controllers = angular.module("controllers");

    controllers.controller("PharmaciesCtrl", ["$scope", "$log", "$location", "$rootScope", "store", "context",
        function($scope, $log, $location, $rootScope, store, context) {
            $scope.pharmacies = null;
            $scope.status = new FormStatus("Caricamento dati in corso...");

            $scope.setMine = function(pharmacy) {
                for (const p of $scope.pharmacies) {
                    if (p.id === pharmacy.id) {
                        p.mine = true;
                        store.updatePharmacy(p,
                            function() {},
                            function(request) {
                                bubbleUpError("Impossibile leggere la lista delle farmacie: il server non ha risposto", request);
                            });
                    } else {
                        p.mine = false;
                    }
                }
            };

            $scope.edit = function(pharmacy) {
                $location.url("/pharmacy/" + pharmacy.id);
            };

            $scope.add = function() {
                $location.url("/pharmacy/_new");
            };

            store.readPharmacies(function(response) {
                if (response.status === 200 && response.data && response.data.length > 0) {
                    $scope.status.readyWithData();
                    $scope.pharmacies = response.data;
                } else {
                    $scope.status.ready("Non sono ancora state definite le farmacie");
                }
            }, function(request) {
                manageError("Impossibile leggere la lista delle farmacie", request);
            });

            $log.log("Init PharmaciesCtrl");

            function manageError(error, request) {
                if (error) {
                    if (request) {
                        if (request.xhrStatus === "timeout") {
                            error += ": il server non ha risposto";
                        } else if (request.xhrStatus) {
                            error += ": il server ha generato un errore";
                        }
                    }

                    $scope.status.error(error);
                }

                context.fromRequest(request);

                $rootScope.$emit("error_occurred", new Error(error));
            }
        }
    ]);

}());