    (function() {
        'use strict';

        var controllers = angular.module("controllers");

        controllers.controller("EditDocumentCtrl", ["$scope", "$log", "$location", "$routeParams", "$q", "$rootScope", "store", "context",
            function($scope, $log, $location, $routeParams, $q, $rootScope, store, context) {
                $scope.document = null;

                $scope.image = {
                    fileName: null,
                    data: "/img/material.io/image-24px.svg"
                };

                $scope.status = new FormStatus("Caricamento dati in corso...");

                function manageError(error, request) {
                    if (error) {
                        if (request) {
                            if (request.xhrStatus === "timeout") {
                                error += ": il server non ha risposto";
                            } else if (request.xhrStatus) {
                                error += ": il server ha generato un errore";
                            }
                        }
                        $scope.status.error(error);
                    }
                    context.fromRequest(request);
                    $rootScope.$emit("error_occurred", new Error(error));
                }

                $scope.exit = function() {
                    var ok = true;

                    if ($scope.status.isDirty) {
                        ok = confirm("Vuoi annullare le modifiche?");
                    }

                    if (ok) {
                        $location.url("/document");
                    }
                };

                $scope.save = function() {
                    $log.log("Saving");
                    $scope.status.saving("Aggiornamento dati in corso...");

                    // If imageUrl refers to an image different than document's one, user changed it.
                    // So (1) remove the old image, (2) create the new one, (3) upsert the document.

                    updateImage().then(function() {
                        storeDocument();
                    });
                };

                $scope.delete = function() {
                    if (confirm("Vuoi cancellare il documento?")) {
                        if ($scope.document.file_name) {
                            store.deleteImage($scope.document.file_name);
                        }

                        store.deleteDocument($scope.document.id, function(response) {
                            if (response.status === 200) {
                                $log.log("Deleted document " + $scope.document.id);
                                $location.url("/document");
                            }
                        }, function(request) {
                            if (request && request.status === 404) {
                                $log.log("Document " + $scope.document.id + " already deleted");
                                $location.url("/document");
                            } else {
                                manageError("Impossibile cancellare il documento", request);
                            }
                        });
                    }
                };

                function init() {
                    if ($routeParams.id === "_new") {
                        $scope.document = {
                            id: null,
                            description: null,
                            file_name: null,
                            publish: null,
                            revoke: null
                        };
                        $scope.status.ready();
                    } else {
                        store.readDocument($routeParams.id, function(response) {
                            $scope.document = response.data;

                            $scope.image = {
                                fileName: response.data.file_name,
                                data: "http://localhost:8000/thumbs/" + response.data.file_name
                            };

                            $scope.status.readyWithData();
                        }, function(request) {
                            manageError("Impossibile leggere il documento", request);
                        });
                    }

                    $log.log("Init EditDocumentCtrl");
                }

                function updateImage() {
                    return $q(function(resolve, reject) {
                        // If imageUrl refers to an image different than document's one, user changed it.
                        // So (1) remove the old image and (2) create the new one.

                        if (documentNeedsUpdate()) {
                            if ($scope.image && $scope.image.data) {
                                if ($scope.image.data.length > 5242880) {
                                    $scope.status.message = "L'immagine supera il limite massimo di 5 MB. Usane una più piccola.";
                                    reject();
                                    return;
                                }
                            }

                            if ($scope.document.file_name) {
                                store.deleteImage($scope.document.file_name);
                            }

                            store.createImage(createBlob(), function() {
                                resolve();
                            }, function(request) {
                                manageError("Impossibile creare la nuova immagine", request);
                                reject();
                            });
                        } else {
                            resolve();
                        }
                    });
                }

                function documentNeedsUpdate() {
                    if (!$scope.image.fileName) {
                        return false;
                    }

                    if (!$scope.document.file_name) {
                        return true;
                    }

                    return $scope.image.fileName != $scope.document.file_name;
                }

                function createBlob() {
                    var comma = $scope.image.data.indexOf(",");

                    return {
                        id: $scope.image.fileName,
                        data: $scope.image.data.substring(comma + 1)
                    };
                }

                function storeDocument() {
                    if ($scope.document.id === null) {
                        store.createDocument({
                            description: $scope.document.description,
                            publish: $scope.document.publish,
                            revoke: $scope.document.revoke,
                            file_name: $scope.image.fileName
                        }, function() {
                            $scope.status.ready();
                            $location.url("/document");
                        }, function(request) {
                            manageError("Impossibile creare il documento", request);
                        });
                    } else {
                        $scope.document.file_name = $scope.image.fileName;
                        store.updateDocument($scope.document, function() {
                            $scope.status.ready();
                            $location.url("/document");
                        }, function(request) {
                            manageError("Impossibile aggiornare il documento", request);
                        });
                    }
                }

                init();
            }
        ]);

    }());