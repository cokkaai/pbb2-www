// vim: set ts=4 sw=4 expandtab:

(function () {
'use strict';

    var directives = angular.module("directives", []);

    directives.directive("selectOneFile", function() {
        return {
            require: "ngModel",
            link: function postLink(scope, element, attributes, ngModel) {
                element.on("change", function(changeEvent) {
                    var file = changeEvent.target.files[0];
                    
                    if (scope.status.dirty) {
                        scope.status.dirty();
                    }

                    var reader = new FileReader();
                    reader.onload = function (loadEvent) {
                        ngModel.$setViewValue({
                            fileName: file.name,
                            data: loadEvent.target.result
                        });
                    }
                    reader.readAsDataURL(file);
                });
            }
        }
    });
})();

