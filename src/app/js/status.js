// vim: set ts=4 sw=4 expandtab:

class FormStatus {
    constructor(message) {
        this.loading = false;
        this.isReady = false;
        this.isDirty = false;
        this.isError = false;
        this.isSaving = false;
        this.isEmpty = false;
        this.messsage = message ? message : null;

        const LOADING = "loading";
        const READY = "ready";
        const DIRTY = "dirty";
        const SAVING = "saving";
        const ERROR = "ERROR";

        var _status = LOADING;

        this.update = function(status) {
            _status = status;

            this.loading = _status === LOADING;
            this.isReady = _status === READY;
            this.isDirty = _status === DIRTY;
            this.isSaving = _status === SAVING;
            this.isError = _status === ERROR;
        };

        this.ready = function(message) {
            this.update(READY);
            this.isEmpty = true;

            if (message) {
                this.message = message;
            }
        };

        this.readyWithData = function(message) {
            this.update(READY);
            this.isEmpty = false;

            if (message) {
                this.message = message;
            }
        };

        this.error = function(message) {
            this.update(ERROR);

            if (message) {
                this.message = message;
            }
        };

        this.dirty = function(message) {
            this.update(DIRTY);

            if (message) {
                this.message = message;
            }
        };

        this.saving = function(message) {
            this.update(SAVING);
            this.isEmpty = false;

            if (message) {
                this.message = message;
            }
        };
    }
}